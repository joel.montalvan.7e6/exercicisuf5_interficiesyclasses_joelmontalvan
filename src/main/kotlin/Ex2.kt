import java.util.*

enum class RAINBOW (val colorRainbow: String){
    MORADO("MORADO"),
    AZUL("AZUL"),
    VERDE("VERDE"),
    AMARILLO("AMARILLO"),
    NARANJA("NARANJA"),
    ROJO("ROJO")
}

fun main(){
    println("INTRODUCE UN COLOR EN ESPAÑOL:")
    val scanner = Scanner(System.`in`)
    val colorEscogido = scanner.next().uppercase()

    for (i in RAINBOW.values()){
        if (colorEscogido == i.colorRainbow ){
            println(true)
        }
        else println(false)
    }
}

data class Estudiante(
    val nombre: String,
    val nota: NOTAFINAL
)

enum class NOTAFINAL{
    SUSPENSO, APROBADO, BIEN, NOTABLE, EXCELENTE
}

fun main(){
    val estudiante1 = Estudiante("Mar", NOTAFINAL.SUSPENSO)
    val estudiante2 = Estudiante("Joan", NOTAFINAL.EXCELENTE)

    println(estudiante1)
    println(estudiante2)
}
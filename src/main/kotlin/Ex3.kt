abstract class Instrument {
     open fun makeSounds(times: Int) {}
}

class Triangle(val soundTriangle: Int): Instrument() {
    override fun makeSounds(times: Int) {
        repeat(times) {
            when (soundTriangle) {
                1 -> println("TINC")
                2 -> println("TIINC")
                3 -> println("TIIINC")
                4 -> println("TIIIINC")
                5 -> println("TIIIIINC")
            }
        }
    }
}

class Drump(val soundDrump: String): Instrument() {
    override fun makeSounds(times: Int) {
        repeat(times) {
            when (soundDrump) {
                "A" -> println("TAAAM")
                "O" -> println("TOOOM")
                "U" -> println("TUUUM")
            }
        }
    }
}

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}